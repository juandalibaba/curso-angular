import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sort',
  //pure: false
})
export class SortPipe implements PipeTransform {

  transform(value: number[], str?: string): number[] {
    return value.sort((a, b) => {
      if(a > b) return (str == 'desc')? -1 : 1
      if(a < b) return (str == 'desc')? 1 : -1
      if(a = b) return 0   
    })
  }
}
