import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TreeService {

  ctx: CanvasRenderingContext2D
  p1: number
  p2: number
  
  constructor() { }

  setCtx(ctx: CanvasRenderingContext2D){
    this.ctx = ctx
  }

  setParams(p1: number, p2: number){
    this.p1 = p1
    this.p2 = p2
  }

  draw(startX, startY, len, angle) {
    this.ctx.beginPath();
    this.ctx.save();

    this.ctx.translate(startX, startY);
    this.ctx.rotate((angle * Math.PI) / 180);
    this.ctx.moveTo(0, 0);
    this.ctx.lineTo(0, -len);
    this.ctx.stroke();

    if (len < 10) {
      this.ctx.restore();
      return;
    }

    this.draw(0, -len, len * this.p1, -this.p2);
    this.draw(0, -len, len * this.p1, this.p2);

    this.ctx.restore();
  }
}
