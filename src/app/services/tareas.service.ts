import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Tarea } from '../types/tarea-type'

@Injectable({
  providedIn: 'root'
})
export class TareasService {

  urlBase = 'https://jsonplaceholder.typicode.com/todos'
  httpOptions = {
    headers: new HttpHeaders({
      "Content-Type": "application/json"      
    })
  };

  constructor(private http: HttpClient) { }

  getAll(): Observable<Tarea[]>{    
    return this.http.get<Tarea[]>(this.urlBase, this.httpOptions)
  }

  get(id: number): Observable<Tarea>{
    return this.http.get<Tarea>(`${this.urlBase}/${id}`, this.httpOptions)
  }

  create(tarea: Tarea){
    return this.http.post<Tarea>(this.urlBase, tarea, this.httpOptions)
  }

  update(tarea: Tarea){
    return this.http.put<Tarea>(`${this.urlBase}/${tarea.id}`, tarea, this.httpOptions)
  }

  delete(id: number){
    return this.http.delete<Tarea>(`${this.urlBase}/${id}`, this.httpOptions)
  }
}
