import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MandelbrotService {

  canvas: HTMLCanvasElement
  ctx: CanvasRenderingContext2D
  panX = 2
  panY = 1.5
  magnificationFactor = 200
  maxIterations = 100

  constructor() { }

  setCanvas(canvas: HTMLCanvasElement, ctx: CanvasRenderingContext2D){
    this.canvas = canvas
    this.ctx = ctx
  }

  setParams(panX, panY, mF, mI){
    this.panX = panX
    this.panY = panY
    this.magnificationFactor = mF
    this.maxIterations = mI
  }

  checkIfBelongsToMandelbrotSet(x, y) {
    var realComponentOfResult = x;
    var imaginaryComponentOfResult = y;
    for (var i = 0; i < this.maxIterations; i++) {
      var tempRealComponent = realComponentOfResult * realComponentOfResult
        - imaginaryComponentOfResult * imaginaryComponentOfResult
        + x;
      var tempImaginaryComponent = 2 * realComponentOfResult * imaginaryComponentOfResult
        + y;
      realComponentOfResult = tempRealComponent;
      imaginaryComponentOfResult = tempImaginaryComponent;

      // Return a number as a percentage
      if (realComponentOfResult * imaginaryComponentOfResult > 5)
        return (i / this.maxIterations * 100);
    }
    return 0;   // Return zero if in set 
  }

  draw() {
    for (var x = 0; x < this.canvas.width; x++) {
      for (var y = 0; y < this.canvas.height; y++) {
        var belongsToSet =
          this.checkIfBelongsToMandelbrotSet(x / this.magnificationFactor - this.panX,
            y / this.magnificationFactor - this.panY);
        if (belongsToSet == 0) {
          this.ctx.fillStyle = '#000';
          this.ctx.fillRect(x, y, 1, 1); // Draw a black pixel
        } else {
          this.ctx.fillStyle = 'hsl(0, 100%, ' + belongsToSet + '%)';
          this.ctx.fillRect(x, y, 1, 1); // Draw a colorful pixel
        }
      }
    }
  }
}
