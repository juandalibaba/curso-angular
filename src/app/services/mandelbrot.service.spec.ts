import { TestBed, inject } from '@angular/core/testing';

import { MandelbrotService } from './mandelbrot.service';

describe('MandelbrotService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MandelbrotService]
    });
  });

  it('should be created', inject([MandelbrotService], (service: MandelbrotService) => {
    expect(service).toBeTruthy();
  }));
});
