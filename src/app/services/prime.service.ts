import { Injectable } from '@angular/core';
import { GuidService } from './guid.service';

@Injectable({
  providedIn: 'root'
})
export class PrimeService {

  id: string

  constructor(private guiService: GuidService) {
    this.id = this.guiService.guid()
  }

  getId() {
    return this.id
  }

  isPrime(value) {
    for (var i = 2; i < value; i++) {
      if (value % i === 0) {
        return false;
      }
    }
    return value > 1;
  }
}
