import { Injectable, Inject, ElementRef } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class CanvasService {

  constructor(@Inject(DOCUMENT) private document: any) { }

  createCanvas(elRef: ElementRef, width=700, height=600){
    let canvas = this.document.createElement("canvas");
    canvas.width = width;
    canvas.height = height;
    elRef.nativeElement.appendChild(canvas);
    let ctx = canvas.getContext("2d");
    
    return {
      canvas: canvas,
      ctx: ctx
    }
  }
}
