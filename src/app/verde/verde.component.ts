import { Component, OnInit } from '@angular/core';
import { GuidService } from '../services/guid.service';

@Component({
  selector: 'app-verde',
  templateUrl: './verde.component.html',
  styleUrls: ['./verde.component.css'],
  providers: [GuidService]
})
export class VerdeComponent implements OnInit {

  constructor(private servicio: GuidService) { }

  ngOnInit() {
    console.log(this.servicio.getId())
  }

}
