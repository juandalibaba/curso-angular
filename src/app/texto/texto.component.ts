import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-texto',
  templateUrl: './texto.component.html',
  styleUrls: ['./texto.component.css']
})
export class TextoComponent implements OnInit {

  text = "Bienvenido"
  text2 = "Al curso de Angular"
  classt1 = "neon"
  classt2 = "flux"
  constructor() { }

  ngOnInit() {
  }

  changeClass(){
    this.classt1 = this.classt1 == "neon"? "flux" : "neon"
    this.classt2 = this.classt2 == "neon"? "flux" : "neon"
  }

}
