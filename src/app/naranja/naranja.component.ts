import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-naranja',
  templateUrl: './naranja.component.html',
  styleUrls: ['./naranja.component.css']
})
export class NaranjaComponent implements OnInit {


  mensaje = "Esto es un mensaje"
  error = "Esto es un mensaje de error"
  var1 = 20
  var2 = 30
  isHidden = false
  isSpecial = false
  elinput = "la entrada"
  elinput2 = "la entrada"

  constructor() { }

  ngOnInit() {
    this.oscillate()
  }

  updateInput(value){
    this.elinput = value
  }

  oscillate(){
    setInterval(() => {
     // this.isHidden = !this.isHidden
      this.isSpecial = !this.isSpecial      
    }, 1000)
  }
  
}
