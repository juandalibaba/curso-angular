import { Component, OnInit } from "@angular/core";
import { Observable, Observer } from "rxjs";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements OnInit {
  componente = "tree";
  time = Observable.create((observer: Observer<string>) => {
    let counter = 0;
    let i = setInterval(() => {
      observer.next(new Date().toString());
      counter++;
      if (counter >= 5) {
        clearInterval(i);
        observer.complete();
      }
    }, 1000);
  });

  arr = [2, 1, 3, 5, 4];
  kaka = "hola";

  gender: string = "male";
  inviteMap: any = {
    male: "Invite him.",
    female: "Invite her.",
    other: "Invite them."
  };

  m1 = true;
  m2 = true;
  m3 = true;

  constructor() {}

  ngOnInit() {
    //this.service.getAlgo('app.component')

    setTimeout(() => {
      this.arr.push(3);
      //this.arr = [2,1,3,5,4,3]
      this.kaka = "adios";
      this.inviteMap["kaka"] = "koko";
    }, 2000);
  }

  delete(event) {
    console.log(event);
    switch (event) {
      case "m1":
        this.m1 = false;
        break;
      case "m2":
        this.m2 = false;
        break;
      case "m3":
        this.m3 = false;
        break;
    }
  }
}
