import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { Observable, from, of, interval, fromEvent, pipe } from "rxjs";
import { ajax } from "rxjs/ajax";
import { filter, map, catchError, retry } from "rxjs/operators";

@Component({
  selector: "app-observables",
  templateUrl: "./observables.component.html",
  styleUrls: ["./observables.component.css"]
})
export class ObservablesComponent implements OnInit {
  @ViewChild("box") box: ElementRef;

  box1 = document.getElementById("kuku") as HTMLElement;

  constructor() {}

  ngOnInit() {}

  ngAfterViewInit() {
    
    let o1 = of("Hola que tal");

    //let o1 = from([1,2,3,"Hola que tal"])

    //let o1 = interval(1000)

    //let o1 = fromEvent(document.getElementById('box'), "mousemove")

    //let o1 = ajax("https://jsonplaceholder.typicode.com/albums")

    // let o1 = Observable.create(observer => {
    //   let n = 0;
    //   setInterval(() => {
    //     n++;
    //     if (n > 5) {
    //       observer.complete()
    //       //observer.error("Mayor que 5!!!!!");
    //     } else {
    //       observer.next("Hola caracola " + n);
    //     }
    //   }, 1000);
    // });

    ////////////// OPERADORES /////////////

    //let o1 = from([1, 2, 3, 4, 5, 6, 7, 8, 9]).pipe(map(v => v * v));

    // let o1 = from([1,2,3,4,5,6,7,8,9]).pipe(
    //   map(v => v*v),
    //   filter(v => v % 2 == 0)
    // )

    // let o1 = fromEvent(document.getElementById('box'), "mousemove").pipe(
    //   map(v => [v.clientX, v.clientY])
    // )

    // let o1 = fromEvent(document.getElementById('box'), "mousemove").pipe(
    //   map((v: MouseEvent) => [v.clientX, v.clientY]),
    //   filter(v => v[0] > 300 && v[1] > 350)
    // )

    // let o1 = from([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]).pipe(
    //   map(v => {
    //     if (v == 5) {
    //       throw new Error("Oh no el 5!!!");
    //     }
    //     return v * v;
    //   })
    // );

    // let arr = [0,1,2,3,4,5,6,7,8,9]
    // let o1 = from(arr).pipe(
    //   retry(3),
    //   map(v => {
    //     if(v == 5){
    //       throw new Error('Oh no el 5!!!')
    //     }
    //     return v*v
    //   }),
    //  // filter(v => v % 2 == 0),
    //   catchError(err => {
    //     return of("cambio el 5 por este mensaje")
    //   })
    // )

    // let o1 = ajax('https://jsonplaceholder.typicode.com/albums').pipe(
    //   map(v => v.response),
    //   map(v => from(v))
    // )

    console.log("suscripción 1");
    o1.subscribe({
      next(v) {
        console.log(v);
      },
      error(e) {
        console.log("error: " + e);
      },
      complete() {
        console.log("completed");
      }
    });

    console.log("suscripción 2");
    o1.subscribe(
      v => {
        console.log(v);
      },
      e => {
        console.log(e);
      },
      () => {
        console.log("completed");
      }
    );
  }
}
