import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { NaranjaComponent } from './naranja/naranja.component';
import { TomateComponent } from './tomate/tomate.component';
import { FormsModule } from '@angular/forms';
import { ImagenComponent } from './imagen/imagen.component';
import { TextoComponent } from './texto/texto.component'
import { FractalsModuleModule } from './fractals-module/fractals-module.module';
import { VecesDirective } from './directives/veces.directive';
import { RotarDirective } from './directives/rotar.directive';
import { SortPipe } from './pipes/sort.pipe';
import { AzulComponent } from './azul/azul.component';
import { VerdeComponent } from './verde/verde.component';
import { GuidService } from './services/guid.service';
import { ObservablesComponent } from './observables/observables.component';
import { TareasComponent } from './tareas/tareas.component';


@NgModule({
  declarations: [
    AppComponent,
    NaranjaComponent,
    TomateComponent,
    ImagenComponent,
    TextoComponent,
    VecesDirective,
    RotarDirective,
    SortPipe,
    AzulComponent,
    VerdeComponent,
    ObservablesComponent,
    TareasComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    FractalsModuleModule,
    HttpClientModule 
  ],
  providers: [GuidService],
  bootstrap: [AppComponent]
})
export class AppModule { }
