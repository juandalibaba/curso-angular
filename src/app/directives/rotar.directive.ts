import {
  Directive,
  ElementRef,
  Input,
  OnInit,
  HostListener
} from "@angular/core";
import { PARAMETERS } from "@angular/core/src/util/decorators";

@Directive({
  selector: "[appRotar]"
})
export class RotarDirective implements OnInit {
  @Input("appRotar") angulo: number;
  incremento: number;
  interval = null

  constructor(private el: ElementRef) {}

  ngOnInit() {
    this.incremento = this.angulo;
    this.el.nativeElement.style.transform = `rotate(${this.angulo}deg)`;
  }

  @HostListener("mouseover")
  girar() {
    this.angulo += this.incremento;
      this.el.nativeElement.style.transform = `rotate(${this.angulo}deg)`;
  }

  // @HostListener("mouseout") parar(){
  //   clearInterval(this.interval);
  // }
}
