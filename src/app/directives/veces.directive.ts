import { Directive, TemplateRef, ViewContainerRef, Input } from '@angular/core';

@Directive({
  selector: '[appVeces]'
})
export class VecesDirective {

  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef
  ) { }

  @Input() set appVeces(n: Number){
    for(let i = 0; i < n; i++){
      this.viewContainer.createEmbeddedView(this.templateRef)
    }
  }

}
