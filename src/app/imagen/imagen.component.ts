import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-imagen',
  templateUrl: './imagen.component.html',
  styleUrls: ['./imagen.component.css']
})
export class ImagenComponent implements OnInit {

  imagenes = [
    "https://image.freepik.com/free-vector/vector-illustration-cosmonaut_1441-11.jpg",
    "https://image.freepik.com/free-vector/polygonal-lion-head_23-2147495868.jpg",
    "https://image.freepik.com/free-vector/hand-painted-steampunk-man-illustration_23-2147537528.jpg",
    "https://image.freepik.com/free-vector/analytical-and-creative-brain_23-2147506845.jpg",
    "https://image.freepik.com/free-vector/abstract-floral-background_1005-10.jpg",
    "https://image.freepik.com/free-vector/thank-you-composition-in-comic-style_23-2147831785.jpg",
    "https://media.giphy.com/media/3ornk2zKMUgETO4aMo/giphy.gif"
  ]

  imagenurl: string

  constructor() {  }

  ngOnInit() {
    this.imagenurl = this.randomImage()
    console.log(this.imagenurl)
  }

  randomImage(){
    let index = Math.floor(Math.random() * this.imagenes.length)
    return this.imagenes[index]
  }

  changeImage(){
    this.imagenurl = this.randomImage()
  }
}
