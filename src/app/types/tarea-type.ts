export type Tarea = {
  userId: number;
  id?: number;
  title: string;
  completed: boolean;
};
