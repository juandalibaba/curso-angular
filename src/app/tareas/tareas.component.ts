import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  ApplicationRef
} from "@angular/core";
import { TareasService } from "../services/tareas.service";
import { Tarea } from "../types/tarea-type";

@Component({
  selector: "app-tareas",
  templateUrl: "./tareas.component.html",
  styleUrls: ["./tareas.component.css"]
})
export class TareasComponent implements OnInit {
  tareas: Tarea[];
  @ViewChild("idTaskBox") idTaskBox: ElementRef;
  @ViewChild("userIdBox") userIdBox: ElementRef;
  @ViewChild("titleBox") titleBox: ElementRef;
  @ViewChild("completedBox") completedBox: ElementRef;
  @ViewChild("idTaskBoxUp") idTaskBoxUp: ElementRef;
  @ViewChild("userIdBoxUp") userIdBoxUp: ElementRef;
  @ViewChild("titleBoxUp") titleBoxUp: ElementRef;
  @ViewChild("completedBoxUp") completedBoxUp: ElementRef;
  @ViewChild("idTaskBoxDel") idTaskBoxDel: ElementRef;
  currentTarea: Tarea; 

  constructor(
    private tareasService: TareasService,
    private appRef: ApplicationRef
  ) {}

  getTarea() {
    let idTarea = this.idTaskBox.nativeElement.value;
    if (idTarea == "") {
      alert("Hay que introducir un número");
      return;
    }
    let that = this;
    this.tareasService.get(idTarea).subscribe({
      next(t) {
        that.currentTarea = t;
      },
      error(e) {
        alert(e.message);
      }
    });
  }

  createTarea() {
    let tarea: Tarea = {
      userId: this.userIdBox.nativeElement.value,
      title: this.titleBox.nativeElement.value,
      completed: this.userIdBox.nativeElement.checked
    };

    let that = this;
    this.tareasService.create(tarea).subscribe({
      next(t) {
        tarea.id = t.id;
        that.tareas.push(tarea);
      },
      error(e) {
        alert(e.message);
      }
    });
    console.log(this.completedBox);
  }

  findObjectByKey(array, key, value) {
    for (var i = 0; i < array.length; i++) {
      if (array[i][key] === value) {
        return i;
      }
    }
    return null;
  }

  updateTarea() {
    let tarea: Tarea = {
      id: this.idTaskBoxUp.nativeElement.value,
      userId: this.userIdBoxUp.nativeElement.value,
      title: this.titleBoxUp.nativeElement.value,
      completed: this.completedBoxUp.nativeElement.checked
    };

    let that = this;
    this.tareasService.update(tarea).subscribe({
      next(t) {
        let tareaIndex = that.findObjectByKey(that.tareas, "id", t.id);
        that.tareas[tareaIndex] = t;
        // esto hace falta porque Angula no detecta cambios en el
        // contenido del array.
        that.appRef.tick();
      },
      error(e) {
        console.log(e);
        alert(e.message);
      }
    });
    console.log(this.completedBox);
  }

  borrarTarea() {
    let that = this;
    let id = this.idTaskBoxDel.nativeElement.value;
    this.tareasService.delete(id).subscribe({
      next(t) {
        let tareaIndex = that.findObjectByKey(that.tareas, "id", parseInt(id));
        console.log(tareaIndex);
        that.tareas.splice(tareaIndex, 1);
        that.appRef.tick();
      },
      error(e) {
        alert(e.message);
      }
    });
  }
  ngOnInit() {
    this.tareasService.getAll().subscribe(tareas => {
      this.tareas = tareas;
    });
  }
}
