import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MandelbrotWithServiceComponent } from './mandelbrot-with-service.component';

describe('MandelbrotWithServiceComponent', () => {
  let component: MandelbrotWithServiceComponent;
  let fixture: ComponentFixture<MandelbrotWithServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MandelbrotWithServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MandelbrotWithServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
