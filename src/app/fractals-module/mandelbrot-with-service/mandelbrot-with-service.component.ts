import { Component, OnInit, Input, Output, EventEmitter, ElementRef } from '@angular/core';
import { CanvasService } from '../../services/canvas.service';
import { MandelbrotService } from '../../services/mandelbrot.service';

@Component({
  selector: 'app-mandelbrot-with-service',
  templateUrl: './mandelbrot-with-service.component.html',
  styleUrls: ['./mandelbrot-with-service.component.css']
})
export class MandelbrotWithServiceComponent implements OnInit {

  canvas: HTMLCanvasElement
  ctx: CanvasRenderingContext2D

  @Input('panX') panX = 2
  @Input('panY') panY = 1.5
  @Input('magnificationFactor') magnificationFactor = 200
  @Input('maxIterations') maxIterations = 100
  @Input('mId') mId = 'mandelbrot'
  @Output('onMandelbrotDelete') onMandelbrotDelete = new EventEmitter<string>()

  constructor(
    private elRef: ElementRef,
    private canvasService: CanvasService,
    private mandelbrotService: MandelbrotService
  ) { }

  ngOnInit() {
    let { canvas, ctx } = this.canvasService.createCanvas(this.elRef)
    this.canvas = canvas,
      this.ctx = ctx

    this.mandelbrotService.setCanvas(this.canvas, this.ctx)
    this.mandelbrotService.setParams(this.panX,
      this.panY,
      this.magnificationFactor,
      this.maxIterations)
    this.mandelbrotService.draw()
  }

  delete() {
    this.onMandelbrotDelete.emit(this.mId)
  }
}
