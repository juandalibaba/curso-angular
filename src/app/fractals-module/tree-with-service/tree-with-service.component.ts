import { Component, OnInit, ElementRef, Input } from '@angular/core';
import { CanvasService } from '../../services/canvas.service';
import { TreeService } from '../../services/tree.service';

@Component({
  selector: 'app-tree-with-service',
  templateUrl: './tree-with-service.component.html',
  styleUrls: ['./tree-with-service.component.css']
})
export class TreeWithServiceComponent implements OnInit {

  ctx: CanvasRenderingContext2D
  @Input('p1') p1 = 0.8;
  @Input('p2') p2 = 15;

  constructor(
    private elRef: ElementRef,
    private canvasService: CanvasService,
    private treeService: TreeService
  ) { }

  ngOnInit() {
    let { ctx } = this.canvasService.createCanvas(this.elRef)
    this.treeService.setCtx(ctx)
    this.treeService.setParams(this.p1, this.p2)
    this.treeService.draw(350, 600, 120, 0);
  }

}
