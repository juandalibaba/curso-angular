import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TreeWithServiceComponent } from './tree-with-service.component';

describe('TreeWithServiceComponent', () => {
  let component: TreeWithServiceComponent;
  let fixture: ComponentFixture<TreeWithServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TreeWithServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreeWithServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
