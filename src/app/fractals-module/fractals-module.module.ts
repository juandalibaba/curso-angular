import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TreeComponent } from './tree/tree.component';
import { FormsModule } from '@angular/forms';
import { MandelbrotComponent } from './mandelbrot/mandelbrot.component';
import { MandelbrotWithServiceComponent } from './mandelbrot-with-service/mandelbrot-with-service.component';
import { TreeWithServiceComponent } from './tree-with-service/tree-with-service.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [
    TreeComponent,
    MandelbrotComponent,
    TreeWithServiceComponent,
    MandelbrotWithServiceComponent
  ],
  exports: [
    TreeComponent,
    MandelbrotComponent,
    TreeWithServiceComponent,
    MandelbrotWithServiceComponent
  ],
  providers: []
})
export class FractalsModuleModule { }
