import { FractalsModuleModule } from './fractals-module.module';

describe('FractalsModuleModule', () => {
  let fractalsModuleModule: FractalsModuleModule;

  beforeEach(() => {
    fractalsModuleModule = new FractalsModuleModule();
  });

  it('should create an instance', () => {
    expect(fractalsModuleModule).toBeTruthy();
  });
});
