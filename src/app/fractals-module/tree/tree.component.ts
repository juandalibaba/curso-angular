import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-tree",
  templateUrl: "./tree.component.html",
  styleUrls: ["./tree.component.css"]
})
export class TreeComponent implements OnInit {
  ctx: any;
  canvas: any;
  p1 = 0.8;
  p2 = 15;

  constructor() {}

  ngOnInit() {
    this.canvas = document.createElement("canvas");
    this.canvas.width = 700;
    this.canvas.height = 600;
    document.getElementById("canvas").appendChild(this.canvas);
    this.ctx = this.canvas.getContext("2d");
    this.draw(350, 600, 120, 0);
  }

  changeParam1(e) {
    if (e.key == "Enter") {
      this.p1 = e.target.value;
      this.clearCanvas()
      this.draw(350, 600, 120, 0);
    }
  }

  changeParam2(e) {
    if (e.key == "Enter") {
      this.p2 = e.target.value;
      this.clearCanvas()
      this.draw(350, 600, 120, 0);
    }
  }

  clearCanvas() {
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
  }

  draw(startX, startY, len, angle) {
    this.ctx.beginPath();
    this.ctx.save();

    this.ctx.translate(startX, startY);
    this.ctx.rotate((angle * Math.PI) / 180);
    this.ctx.moveTo(0, 0);
    this.ctx.lineTo(0, -len);
    this.ctx.stroke();

    if (len < 10) {
      this.ctx.restore();
      return;
    }

    this.draw(0, -len, len * this.p1, -this.p2);
    this.draw(0, -len, len * this.p1, this.p2);

    this.ctx.restore();
  }
}
