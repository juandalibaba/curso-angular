import { Component, OnInit } from '@angular/core';
import { GuidService } from '../services/guid.service';
import { PrimeService } from '../services/prime.service';

@Component({
  selector: 'app-azul',
  templateUrl: './azul.component.html',
  styleUrls: ['./azul.component.css']
})
export class AzulComponent implements OnInit {

  constructor(
    private servicio: GuidService,
    private primeService: PrimeService) { }

  ngOnInit() {
    console.log(this.servicio.getId())
    console.log(this.primeService.getId())
  }

}
