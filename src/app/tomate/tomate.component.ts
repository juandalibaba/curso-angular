import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tomate',
  templateUrl: './tomate.component.html',
  styleUrls: ['./tomate.component.css']
})
export class TomateComponent implements OnInit {

  arr = [6,5,4,3,2,1]
  a = 5
  constructor() { }

  ngOnInit() {

  }

  change(){
    this.a = 45
    this.arr.push(5)
  }

}
